/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package bainhom_nguyenthuy;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.LinkedHashMap;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
/**
 *
 * @author hiimneo
 */
public class BaiNhom_NguyenThuy {

    /**
     * @param args the command line arguments
     */
    
    static int index = 0;
    
    public static void themTV(LinkedHashMap<Integer, String> x){
        Scanner sc = new Scanner(System.in);
        String tv;
        System.out.print("Nhap ten thanh vien: ");
        tv = sc.nextLine();
        int id = index;
        x.put(id, tv);
        index++;
    }
    
    public static void suaTV(LinkedHashMap<Integer, String> x){
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ten thanh vien muon sua: ");
        String tv = sc.nextLine();
        System.out.print("Nhap ten thanh vien moi: ");
        String tvm = sc.nextLine();
        int index = -1;
        for(Integer key : x.keySet()){
            if(tv.equals(x.get(key))){
                index = key;
            }
        }
        if(index == -1){
            System.out.println("Khong co thanh vien ten " + tv + " trong danh sach");
        } else{
            x.replace(index, tvm);
            System.out.println("SUA THANH CONG!");
            hienThi(x);
        }        
    }
    
    public static void xoaTV(LinkedHashMap<Integer, String> x){
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ten thanh vien muon xoa: ");
        String tv = sc.nextLine();
        int index = -1;
        for(Integer key : x.keySet()){
            if(tv.equals(x.get(key))){
                index = key;
            }
        }
        if(index == -1){
            System.out.println("Khong co thanh vien ten " + tv + " trong danh sach");
        } else{
            x.remove(index);
            System.out.println("XOA THANH CONG!");
            hienThi(x);
        }   
    }
    
    public static void timKiem(LinkedHashMap<Integer, String> x){
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap ten thanh vien muon tim kiem: ");
        String tv = sc.nextLine();
        int index = -1;
        for(Integer key : x.keySet()){
            if(tv.equals(x.get(key))){
                index = key;
            }
        }
        if(index == -1){
            System.out.println("Khong co thanh vien ten " + tv + " trong danh sach");
        } else{
            System.out.printf("%-3s %20s\n", "ID", "Ho va ten");
            System.out.printf("%-3d %20s\n",index, x.get(index));
        }
        
    }
    
    public static void sapXep(LinkedHashMap<Integer, String> x){
        ArrayList<Map.Entry<Integer, String>> list = new ArrayList<Map.Entry<Integer, String>>(x.entrySet());
        Comparator<Map.Entry<Integer, String>> o = new Comparator<Map.Entry<Integer, String>>(){
            @Override
            public int compare(Map.Entry<Integer, String> o1, Map.Entry<Integer, String> o2) {
                return o1.getValue().compareToIgnoreCase(o2.getValue());
            }  
        };
        Collections.sort(list, o);
        System.out.printf("%-3s %20s\n", "ID", "Ho va ten");
        for (Map.Entry<Integer, String> l : list) {
            System.out.printf("%-3d %20s\n", l.getKey(), l.getValue());
        }
    }
    
    public static void hienThi(LinkedHashMap<Integer, String> x){
        System.out.printf("%-3s %20s\n", "ID", "Ho va ten");
        for(Integer key : x.keySet()){
            System.out.printf("%-3d %20s\n", key, x.get(key));
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        LinkedHashMap<Integer, String> ds = new LinkedHashMap<Integer, String>();
        Scanner sc= new Scanner(System.in);
        int chon;
        do{
            System.out.println("==============LinkedHashMap - Nhom 6==============");
            System.out.println("1\tThem");
            System.out.println("2\tSua");
            System.out.println("3\tXoa");
            System.out.println("4\tTim Kiem");
            System.out.println("5\tHien Thi");
            System.out.println("6\tSap xep theo ten");
            System.out.println("7\tThoat");
            System.out.print("Nhap lua chon: ");
            chon=sc.nextInt();
            switch(chon){
            case 1:
                themTV(ds);
                break;
            case 2:
                suaTV(ds);
                 break;
            case 3:
                xoaTV(ds);
                break;
            case 4:
                timKiem(ds);
                break;
            case 5:
                hienThi(ds);
                break;
            case 6:
                sapXep(ds);
                break;
            }
       }    while(chon!=7);
        
    }
    
}
