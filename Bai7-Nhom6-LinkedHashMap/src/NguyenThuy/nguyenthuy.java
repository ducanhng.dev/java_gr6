/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/File.java to edit this template
 */
package NguyenThuy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author ducanhit
 */
public class nguyenthuy {

    /**
     * @param args the command line arguments
     */
    static int index = 0;

    public static void themTV(LinkedHashMap<Integer, Integer> x) {
        Scanner sc = new Scanner(System.in);
        int tv;
        System.out.print("Nhap phan tu moi: ");
        tv = sc.nextInt();
        int id = index;
        x.put(id, tv);
        index++;
    }

    public static void suaTV(LinkedHashMap<Integer, Integer> x) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap phan tu muon sua: ");
        int tv = sc.nextInt();
        System.out.print("Nhap phan tu moi: ");
        int tvm = sc.nextInt();
        int index = -1;
        for (Integer key : x.keySet()) {
            if (tv == x.get(key)) {
                index = key;
            }
        }
        if (index == -1) {
            System.out.println("Khong co phan tu " + tv + " trong danh sach");
        } else {
            x.replace(index, tvm);
            System.out.println("SUA THANH CONG!");
            hienThi(x);
        }
    }

    public static void xoaTV(LinkedHashMap<Integer, Integer> x) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap phan tu muon xoa: ");
        int tv = sc.nextInt();
        int index = -1;
        for (Integer key : x.keySet()) {
            if (tv == x.get(key)) {
                index = key;
            }
        }
        if (index == -1) {
            System.out.println("Khong co phan tu " + tv + " trong danh sach");
        } else {
            x.remove(index);
            System.out.println("XOA THANH CONG!");
            hienThi(x);
        }
    }

    public static void timKiem(LinkedHashMap<Integer, Integer> x) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap phan tu muon tim kiem: ");
        int tv = sc.nextInt();
        int index = -1;
        for (Integer key : x.keySet()) {
            if (tv == x.get(key)) {
                index = key;
            }
        }
        if (index == -1) {
            System.out.println("Khong co phan tu " + tv + " trong danh sach");
        } else {
            System.out.printf("%-3s %20s\n", "ID", "Gia tri");
            System.out.printf("%-3d %20d\n", index, x.get(index));
        }

    }

    public static void sapXep(LinkedHashMap<Integer, Integer> x) {
        ArrayList<Map.Entry<Integer, Integer>> list = new ArrayList<>(x.entrySet());
        Comparator<Map.Entry<Integer, Integer>> o = new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o1.getValue() - o2.getValue();
            }
        };
        Collections.sort(list, o);
        System.out.printf("%-3s %20s\n", "ID", "Gia tri");
        for (Map.Entry<Integer, Integer> l : list) {
            System.out.printf("%-3d %20d\n", l.getKey(), l.getValue());
        }
    }

    public static void hienThi(LinkedHashMap<Integer, Integer> x) {
        System.out.printf("%-3s %20s\n", "ID", "Gia tri");
        for (Integer key : x.keySet()) {
            System.out.printf("%-3d %20d\n", key, x.get(key));
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here

        LinkedHashMap<Integer, Integer> ds = new LinkedHashMap<>();
        Scanner sc = new Scanner(System.in);
        int chon;
        do {
            System.out.println("===LinkedHashMap - Nhom 6 - Du Lieu Nguyen Thuy===");
            System.out.println("1\tThem");
            System.out.println("2\tSua");
            System.out.println("3\tXoa");
            System.out.println("4\tTim Kiem");
            System.out.println("5\tHien Thi");
            System.out.println("6\tSap xep tang dan");
            System.out.println("7\tThoat");
            System.out.print("Nhap lua chon: ");
            chon = sc.nextInt();
            switch (chon) {
                case 1:
                    themTV(ds);
                    break;
                case 2:
                    suaTV(ds);
                    break;
                case 3:
                    xoaTV(ds);
                    break;
                case 4:
                    timKiem(ds);
                    break;
                case 5:
                    hienThi(ds);
                    break;
                case 6:
                    sapXep(ds);
                    break;
            }
        } while (chon != 7);

    }
}
