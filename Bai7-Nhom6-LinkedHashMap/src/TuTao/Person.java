/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/File.java to edit this template
 */
package TuTao;

import java.util.Scanner;

/**
 *
 * @author ducanhit
 */
public class Person {

    /**
     * @param args the command line arguments
     */
    private String personID, name, gender;
    private int age;
    private double salary;

    public Person(String personID, String name, String gender, int age, double salary) {
        this.personID = personID;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.salary = salary;
    }
    
    public Person() {
        this.personID = "";
        this.name = "";
        this.gender = "";
        this.age = 0;
        this.salary = 0;
    }

    public String getPersonID() {
        return personID;
    }

    public String getName() {
        return name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    public int getAge() {
        return age;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void nhap() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhap CMND: ");
            this.personID = sc.nextLine();
            System.out.print("Nhap ten: ");
            this.name = sc.nextLine();
            int temp = 0;
            System.out.print("Nhap gioi tinh(0=Nam, 1=Nu): ");
            temp = sc.nextInt();
            switch (temp) {
                case 0: {
                    this.gender = "Nam";
                    break;
                }
                case 1: {
                    this.gender = "Nu";
                    break;
                }
            }
            System.out.print("Nhap tuoi: ");
            this.age = sc.nextInt();
            System.out.print("Nhap luong: ");
            this.salary = sc.nextDouble();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    @Override
    public String toString() {
        return String.format("%-5s %-20s %-5s %-2d %10f", this.personID, this.name, this.gender, this.age, this.salary);
    }
}
