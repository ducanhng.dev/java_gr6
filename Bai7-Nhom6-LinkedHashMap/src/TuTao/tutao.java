/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package TuTao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Scanner;

/**
 *
 * @author ducanhit
 */
public class tutao {

    /**
     * @param x
     */
    static int index = 1;

    public static void themNV(LinkedHashMap<Integer, Person> x) {
        System.out.println("Nhap thong tin Nhan Vien moi: ");
        Person ps = new Person();
        ps.nhap();
        x.put(index, ps);
        index++;
    }

    public static void suaNV(LinkedHashMap<Integer, Person> x) {
        Person ps = new Person();
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhap ten Nhan Vien can sua: ");
        String nameTemp = sc.nextLine();
        int temp = 0, keyTemp = 0;
        for (int key : x.keySet()) {
            ps = x.get(key);
            if (nameTemp.equals(ps.getName())) {
                keyTemp = key;
                temp++;
            }
        }
        if (temp == 0) {
            System.out.println("Khong co ten Nhan Vien trong danh sach!");
        } else if (temp > 1) {
            System.out.println("Vui long nhap them ID Nhan Vien: ");
            String personIDTemp = sc.nextLine();
            for (int key : x.keySet()) {
                ps = x.get(key);
                if (personIDTemp.equals(ps.getPersonID()) == true && nameTemp.equals(ps.getName()) == true) {
                    System.out.println("Nhap thong tin nhan vien can sua: ");
                    ps.nhap();
                    x.replace(key, ps);
                    break;
                }
            }

        } else if (temp == 1) {
            System.out.println("Nhap thong tin nhan vien can sua: ");
            ps.nhap();
            x.replace(keyTemp, ps);
        }
    }

    public static void xoaNV(LinkedHashMap<Integer, Person> x) {
        Person ps = new Person();
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhap ten Nhan Vien can xoa: ");
        String nameTemp = sc.nextLine();
        int temp = 0, keyTemp = 0;
        for (int key : x.keySet()) {
            ps = x.get(key);
            if (nameTemp.equals(ps.getName())) {
                keyTemp = key;
                temp++;
            }
        }
        if (temp == 0) {
            System.out.println("Khong co ten Nhan Vien trong danh sach!");
        } else if (temp > 1) {
            System.out.println("Vui long nhap them ID Nhan Vien: ");
            String personIDTemp = sc.nextLine();
            for (int key : x.keySet()) {
                ps = x.get(key);
                if (personIDTemp.equals(ps.getPersonID()) && nameTemp.equals(ps.getName())) {
                    x.remove(key, ps);
                    break;
                }
            }
        } else {
            x.remove(keyTemp, ps);
        }
    }

    public static void timKiemNV(LinkedHashMap<Integer, Person> x) {
        Person ps = new Person();
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhap ten Nhan Vien can tim: ");
        String nameTemp = sc.nextLine();
        int temp = 0;
        for (int key : x.keySet()) {
            ps = x.get(key);
            if (nameTemp.equals(ps.getName())) {
                temp++;
            }
        }
        if (temp == 0) {
            System.out.println("Khong co ten Nhan Vien trong danh sach!");
        } else if (temp != 1) {
            System.out.println("Vui long nhap them CMND: ");
            String personIDTemp = sc.nextLine();
            for (int key : x.keySet()) {
                ps = x.get(key);
                if (personIDTemp.equals(ps.getPersonID()) && nameTemp.equals(ps.getName())) {
                    System.out.printf("%-10s %-20s %-8s %-5s %10s\n", "PersonID", "Name", "Sex", "Age", "Salary");
                    System.out.printf("%-10s %-20s %-8s %-5d %10.2f\n", ps.getPersonID(), ps.getName(), ps.getGender(), ps.getAge(), ps.getSalary());
                }
            }

        } else {
            System.out.printf("%-10s %-20s %-8s %-5d %10f\n", ps.getPersonID(), ps.getName(), ps.getGender(), ps.getAge(), ps.getSalary());
        }
    }

    public static void sapXepTheoLuong(LinkedHashMap<Integer, Person> x) {
        ArrayList<Double> list = new ArrayList<>();
        Person pst = new Person();
        for (Person ps : x.values()) {
            list.add(ps.getSalary());
        }
        Collections.sort(list);
        System.out.println("Danh sach Nhan Vien sap xep theo luong tang dan: ");
        System.out.printf("%-10s %-20s %-8s %-5s %10s\n", "PersonID", "Name", "Sex", "Age", "Salary");
        for (Double salary : list) {
            for (Integer key : x.keySet()) {
                pst = x.get(key);
                if (salary == pst.getSalary()) {
                    System.out.printf("%-10s %-20s %-8s %-5d %10.2f\n", pst.getPersonID(), pst.getName(), pst.getGender(), pst.getAge(), pst.getSalary());
                }
            }
        }
    }

    public static void hienThi(LinkedHashMap<Integer, Person> x) {
        Person ps = new Person();
        System.out.printf("%-10s %-20s %-8s %-5s %10s\n", "PersonID", "Name", "Sex", "Age", "Salary");
        for (int key : x.keySet()) {
            ps = x.get(key);
            System.out.printf("%-10s %-20s %-8s %-5d %10.2f\n", ps.getPersonID(), ps.getName(), ps.getGender(), ps.getAge(), ps.getSalary());
        }
    }

    public static void main(String[] args) {
        LinkedHashMap<Integer, Person> lmap = new LinkedHashMap<>();
        Scanner sc = new Scanner(System.in);

        System.out.println("===LinkedHashMap - Nhom 6 - Du Lieu Tu Tao===");
        int chon;
        do {
            System.out.println("1\tThem");
            System.out.println("2\tSua");
            System.out.println("3\tXoa");
            System.out.println("4\tTim kiem");
            System.out.println("5\tHien thi");
            System.out.println("6\tSap xep theo luong");
            System.out.println("7\tThoat");
            System.out.print("Vui long chon yeu cau: ");
            chon = sc.nextInt();
            switch (chon) {
                case 1:
                    themNV(lmap);
                    break;
                case 2:
                    suaNV(lmap);
                    break;
                case 3:
                    xoaNV(lmap);
                    break;
                case 4:
                    timKiemNV(lmap);
                    break;
                case 5:
                    hienThi(lmap);
                    break;
                case 6:
                    sapXepTheoLuong(lmap);
                    break;
            }
        } while (chon != 7);
    }

}
